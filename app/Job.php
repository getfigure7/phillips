<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'referral_date',
        'job_date',
        'completed_date',
        'referral_type',
        'job_status',
        'job_type',
        'commission',
        'pricing',
        'repairs',
        'repair_type',
        'paint_type',
        'gal_amt',
        'paint_cost',
        'misc_material_cost',
        'material_cost',
        'subs',
        'supervisor',
        'labor_gross',
        'total_cost',
        'net_income',
        'profit',
        'profit_margin',
        'job_notes',
        'cost_per_gal',
        'sub_labor',
        'emp_labor',
        'job_month',
        'job_year',
        'job_location',
        'days_to_complete',
        'rays_jobs',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
