<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class UsersController extends Controller
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$this->middleware('auth');
	}

	/**
     * Shows a list of users
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$users = User::orderBy('created_at', 'desc')->paginate(10);

		return view('users.index', compact('users'));
	}

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ];

        $user = User::create($data);

		return redirect('/users/'.$user->id.'/edit')
			->withSuccess('You have successfully created this user.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

		return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$user = User::find($id);

		$user->update([
			'is_active' => $request->is_active,
			'name' => $request->name,
			'email' => $request->email,
		]);

		// Update the User password if a new one has been passed in request
		if ($request->password) $user->update([ 'password' => $request->password, ]);

		return redirect('/users/'.$id.'/edit')
			->withSuccess('You have successfully updated this user.');
    }


}
