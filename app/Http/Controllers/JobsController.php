<?php

namespace App\Http\Controllers;

use App\Job;
use App\Customer;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class JobsController extends Controller
{
	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$this->middleware('auth');
	}

	/**
     * Shows a list of jobs.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$jobs = Job::orderBy('created_at', 'desc')->paginate(10);

		return view('jobs.index', compact('jobs'));
	}

    /**
     * Show the form for creating a new job.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::orderBy('created_at', 'desc')->pluck('email', 'id');

        return view('jobs.create', compact('customers'));
    }

    /**
     * Store a newly created job in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'customer_id' => $request->customer_id,
            'referral_date' => $request->referral_date,
            'job_date' => $request->job_date,
            'completed_date' => $request->completed_date,
            'referral_type' => $request->referral_type,
            'job_status' => $request->job_status,
            'job_type' => $request->job_type,
            'commission' => $request->commission,
            'pricing' => $request->pricing,
            'repairs' => $request->repairs,
            'repair_type' => $request->repair_type,
            'paint_type' => $request->paint_type,
            'gal_amt' => $request->gal_amt,
            'paint_cost' => $request->paint_cost,
            'misc_material_cost' => $request->misc_material_cost,
            'material_cost' => $request->material_cost,
            'subs' => $request->subs,
            'supervisor' => $request->supervisor,
            'labor_gross' => $request->labor_gross,
            'total_cost' => $request->total_cost,
            'net_income' => $request->net_income,
            'profit' => $request->profit,
            'profit_margin' => $request->profit_margin,
            'job_notes' => $request->job_notes,
            'cost_per_gal' => $request->cost_per_gal,
            'sub_labor' => $request->sub_labor,
            'emp_labor' => $request->emp_labor,
            'job_month' => $request->job_month,
            'job_year' => $request->job_year,
            'job_location' => $request->job_location,
            'days_to_complete' => $request->days_to_complete,
            'rays_jobs' => $request->rays_jobs,
        ];

        $job = Job::create($data);

		return redirect('/jobs/'.$job->id.'/edit')
			->withSuccess('You have successfully created this job.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);

        $customers = Customer::orderBy('created_at', 'desc')->pluck('email', 'id');

		return view('jobs.edit', compact('job', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$job = Job::find($id);

		$job->update([
            'customer_id' => $request->customer_id,
            'referral_date' => $request->referral_date,
            'job_date' => $request->job_date,
            'completed_date' => $request->completed_date,
            'referral_type' => $request->referral_type,
            'job_status' => $request->job_status,
            'job_type' => $request->job_type,
            'commission' => $request->commission,
            'pricing' => $request->pricing,
            'repairs' => $request->repairs,
            'repair_type' => $request->repair_type,
            'paint_type' => $request->paint_type,
            'gal_amt' => $request->gal_amt,
            'paint_cost' => $request->paint_cost,
            'misc_material_cost' => $request->misc_material_cost,
            'material_cost' => $request->material_cost,
            'subs' => $request->subs,
            'supervisor' => $request->supervisor,
            'labor_gross' => $request->labor_gross,
            'total_cost' => $request->total_cost,
            'net_income' => $request->net_income,
            'profit' => $request->profit,
            'profit_margin' => $request->profit_margin,
            'job_notes' => $request->job_notes,
            'cost_per_gal' => $request->cost_per_gal,
            'sub_labor' => $request->sub_labor,
            'emp_labor' => $request->emp_labor,
            'job_month' => $request->job_month,
            'job_year' => $request->job_year,
            'job_location' => $request->job_location,
            'days_to_complete' => $request->days_to_complete,
            'rays_jobs' => $request->rays_jobs,
        ]);

		return redirect('/jobs/'.$id.'/edit')
			->withSuccess('You have successfully updated this job.');
    }


}
