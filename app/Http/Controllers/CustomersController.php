<?php

namespace App\Http\Controllers;

use App\Customer;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class CustomersController extends Controller
{
	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$this->middleware('auth');
	}

	/**
     * Shows a list of customers.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$customers = Customer::orderBy('created_at', 'desc')->paginate(10);

		return view('customers.index', compact('customers'));
	}

    /**
     * Show the form for creating a new customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'email' => $request->email
        ];

        $customer = Customer::create($data);

		return redirect('/customers/'.$customer->id.'/edit')
			->withSuccess('You have successfully created this customer.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

		return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$customer = Customer::find($id);

		$customer->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'email' => $request->email
        ]);

		return redirect('/customers/'.$id.'/edit')
			->withSuccess('You have successfully updated this customer.');
    }


}
