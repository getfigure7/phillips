<div class="col-md-3">
    <div class="list-group">
        <span href="#" class="list-group-item sidebar-heading">
            Manage Jobs
        </span>
        <a href="/jobs" class="list-group-item  {{$selected == 'ViewAll' ? 'new-active' : '' }}">
            <i class="fa fa-comment-o"></i>View All
        </a>
        <a href="/jobs/create" class="list-group-item {{ $selected == 'Create' ? 'new-active' : '' }}">
            <i class="fa fa-search"></i>Create Job
        </a>
    </div>
</div>