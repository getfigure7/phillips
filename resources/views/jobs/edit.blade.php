@extends('layouts.app')

@section('content')
<div class="container">
    @include('jobs.partials.sidebar', ['selected' => 'Edit'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Edit Job</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/jobs/{{ $job->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group required">
                        {!! Form::label('customer_id', 'Customer', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('customer_id', $customers, old('customer_id') ?: $job->customer_id, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Referral Date</label>
                        <div class="col-md-6">
                            <input type="datetime" class="form-control" name="referral_date" value="{{ old('referral_date') ?: $job->referral_date }}" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Edit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
        @endsection
