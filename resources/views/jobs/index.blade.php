@extends('layouts.app')

@section('content')
<div class="container">
        @include('jobs.partials.sidebar', ['selected' => 'ViewAll'])
        <div class="col-md-9">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>View All</h4>
                    </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>
                            <span>Created</span>
                        </th>
                        <th>
                            <span>Email</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($jobs as $customer)
                    <tr>
                        <td>
                            <a href="/jobs/{{ $customer->id }}/edit">{{ $customer->first_name }} {{ $customer->last_name }}</a>
                        </td>
                        <td>{{ $customer->created_at }}</td>
                        <td>
                            <a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" style="text-align:center;">There aren't any jobs created.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="pull-right">
                {!! $jobs->render() !!}
            </div>
    </div>
    <br />
</div>
@endsection
