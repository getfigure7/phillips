@extends('layouts.app')

@section('content')
<div class="container">
    @include('jobs.partials.sidebar', ['selected' => 'Create'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Create Job</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/jobs">
                    {{ csrf_field() }}

                    <div class="form-group required">
                        {!! Form::label('customer_id', 'Customer', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('customer_id', $customers, old('customer_id'), ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Referral Date</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="referral_date" value="{{ old('referral_date') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Date</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_date" value="{{ old('job_date') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Completed Date</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="completed_date" value="{{ old('completed_date') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Referral Type</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="referral_type" value="{{ old('referral_type') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Status</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_status" value="{{ old('job_status') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Type</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_type" value="{{ old('job_type') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Commission</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="commission" value="{{ old('commission') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Pricing</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="pricing" value="{{ old('pricing') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Repairs</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="repairs" value="{{ old('repairs') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Repair Type</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="repair_type" value="{{ old('repair_type') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Paint Type</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="paint_type" value="{{ old('paint_type') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Gallon Amount</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="gal_amt" value="{{ old('gal_amt') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Paint Cost</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="paint_cost" value="{{ old('paint_cost') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Misc Material Cost</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="misc_material_cost" value="{{ old('misc_material_cost') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Material Cost</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="material_cost" value="{{ old('material_cost') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Subs</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="subs" value="{{ old('subs') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Supervisor</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="supervisor" value="{{ old('supervisor') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Labor Gross</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="labor_gross" value="{{ old('labor_gross') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Total Cost</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="total_cost" value="{{ old('total_cost') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Net Income</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="net_income" value="{{ old('net_income') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Profit</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="profit" value="{{ old('profit') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Profit Margin</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="profit_margin" value="{{ old('profit_margin') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Notes</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_notes" value="{{ old('job_notes') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Cost Per Gallon</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="cost_per_gal" value="{{ old('cost_per_gal') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Sub Labor</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="sub_labor" value="{{ old('sub_labor') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Emp Labor</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="emp_labor" value="{{ old('emp_labor') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Month</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_month" value="{{ old('job_month') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Year</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_year" value="{{ old('job_year') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Job Location</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="job_location" value="{{ old('job_location') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Days To Complete</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="days_to_complete" value="{{ old('days_to_complete') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Rays Jobs</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="rays_jobs" value="{{ old('rays_jobs') }}" />
                        </div>
                    </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-9">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection
