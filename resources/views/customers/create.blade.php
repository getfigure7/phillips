@extends('layouts.app')

@section('content')
<div class="container">
    @include('customers.partials.sidebar', ['selected' => 'Create'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Create Customer</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/customers">
                    {{ csrf_field() }}

                    <div class="form-group required">
                        <label class="col-md-4 control-label">First Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Address 1</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address1" value="{{ old('address1') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Address 2</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address2" value="{{ old('address2') }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">City</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">State</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="state" value="{{ old('state') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Zip/Postal Code</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="zip" value="{{ old('zip') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-9">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
