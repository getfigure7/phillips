@extends('layouts.app')

@section('content')
<div class="container">
    @include('customers.partials.sidebar', ['selected' => 'Edit'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Edit Customer</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/customers/{{ $customer->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group required">
                        <label class="col-md-4 control-label">First Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') ?: $customer->first_name }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') ?: $customer->last_name }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') ?: $customer->email }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Address 1</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address1" value="{{ old('address1') ?: $customer->address1 }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Address 2</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address2" value="{{ old('address2') ?: $customer->address2 }}" />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">City</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="city" value="{{ old('city') ?: $customer->city }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">State</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="state" value="{{ old('state') ?: $customer->state }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Zip/Postal Code</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="zip" value="{{ old('zip') ?: $customer->zip }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') ?: $customer->phone }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Edit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
        @endsection
