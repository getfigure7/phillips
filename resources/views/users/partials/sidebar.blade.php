<div class="col-md-3">
    <div class="list-group">
        <span href="#" class="list-group-item sidebar-heading">
            Manage Users
        </span>
        <a href="/users" class="list-group-item  {{ $selected == 'ViewAll' ? 'new-active' : '' }}">
            <i class="fa fa-comment-o"></i> View All
        </a>
        <a href="/users/create" class="list-group-item {{ $selected == 'Create' ? 'new-active' : '' }}">
            <i class="fa fa-search"></i> Create User
        </a>
    </div>
</div>