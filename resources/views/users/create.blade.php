@extends('layouts.app')

@section('content')
<div class="container">
    @include('users.partials.sidebar', ['selected' => 'Create'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Create User</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/users">
                    {{ csrf_field() }}

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Full Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-9">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
