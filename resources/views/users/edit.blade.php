@extends('layouts.app')

@section('content')
<div class="container">
    @include('users.partials.sidebar', ['selected' => 'Edit'])
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Edit User</h4>
            </div>
            <div class="panel-body">
                @include('messages.errors')

                <form class="form-horizontal" role="form" method="POST" action="/users/{{ $user->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group required">
                        {!! Form::label('is_active', 'Status', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('is_active', ['1' => 'Active', '0' => 'Inactive'], $user->is_active, ['class' => 'form-control']) !!}
                            <p class="help-block">Active allows the user to login while Inactive does not.</p>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">Full Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?: $user->name }}" required />
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') ?: $user->email }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-md-12">
                            <h5>Change Password (optional)</h5>
                            You can change the user password by entering a new password below.
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Edit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
        @endsection
