@extends('layouts.app')

@section('content')
<div class="container">
        @include('users.partials.sidebar', ['selected' => 'ViewAll'])
        <div class="col-md-9">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>View All</h4>
                    </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>
                            <span>Created</span>
                        </th>
                        <th class="text-center">
                            <span>Status</span>
                        </th>
                        <th>
                            <span>Email</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $user)
                    <tr>
                        <td>
                            <a href="/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                        </td>
                        <td>{{ $user->created_at }}</td>
                        <td class="text-center">
                            @if ($user->is_active)
                            <span class="label label-success">Active</span>@else
                            <span class="label label-danger">Inactive</span>@endif
                        </td>
                        <td>
                            <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" style="text-align:center;">There aren't any users created.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="pull-right">
                {!! $users->render() !!}
            </div>
    </div>
    <br />
</div>
@endsection
