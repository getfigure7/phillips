@if(Session::has('success'))
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            <ul>
                <li>{{ Session::get('success') }}</li>
            </ul>
        </div>
    </div>
</div>
@endif