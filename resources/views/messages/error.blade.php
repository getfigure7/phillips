@if(Session::has('error'))
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <ul>
                <li>{{ Session::get('error') }}</li>
            </ul>
        </div>
    </div>
</div>
@endif
