@if (count($errors) > 0)
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <strong>Wait up..</strong>There were some problems with your input.
            <br />
            <br />
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif