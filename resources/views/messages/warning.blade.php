@if(Session::has('warning'))
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-warning" role="alert">
            <ul>
                <li>{{Session::get('warning') }}</li>
            </ul>
        </div>
    </div>
</div>
@endif