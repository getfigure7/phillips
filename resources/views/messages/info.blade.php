@if(Session::has('info'))
<div class="container">
    <div class="col-md-12">
        <div class="alert alert-info" role="alert">
            <ul>
                <li>{{ Session::get('info') }}</li>
            </ul>
        </div>
    </div>
</div>
@endif