<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->timestamp('referral_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('job_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('completed_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->string('referral_type')->default('');
            $table->string('job_status')->nullable();
            $table->string('job_type')->nullable();
            $table->decimal('commission', 10, 2)->nullable();
            $table->decimal('pricing', 10, 2)->nullable();
            $table->string('repairs')->default('')->nullable();
            $table->string('repair_type')->default('')->nullable();
            $table->string('paint_type')->default('')->nullable();
            $table->integer('gal_amt')->nullable();
            $table->decimal('paint_cost', 10, 2)->nullable();
            $table->decimal('misc_material_cost', 10, 2)->nullable();
            $table->decimal('material_cost', 10, 2)->nullable();
            $table->string('subs')->nullable();
            $table->string('supervisor')->nullable();
            $table->decimal('labor_gross', 10, 2)->nullable();
            $table->decimal('total_cost', 10, 2)->nullable();
            $table->decimal('net_income', 10, 2)->nullable();
            $table->decimal('profit', 10, 2)->nullable();
            $table->decimal('profit_margin', 8, 4)->nullable();
            $table->text('job_notes')->nullable();
            $table->decimal('cost_per_gal', 10, 2)->nullable();
            $table->decimal('sub_labor', 10, 2)->nullable();
            $table->decimal('emp_labor', 10, 2)->nullable();
            $table->integer('job_month')->nullable();
            $table->integer('job_year')->nullable();
            $table->string('job_location')->nullable();
            $table->integer('days_to_complete')->nullable();
            $table->string('rays_jobs')->nullable();

            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
